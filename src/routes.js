import VueRouter from 'vue-router'
import Vue from 'vue'

const routes = [
  {
    path: '/',
    component: () => import('./Home.vue')
  },
  {
    path: '/form',
    component: () => import('./Form.vue')
  }
]

Vue.use(VueRouter)

export default new VueRouter({
  routes,
  linkActiveClass: 'active'
})